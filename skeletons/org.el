(define-skeleton org-skeleton
  "Insert a title and the author."
  "#+title: " str
  "#+author: Tristan Riehs")
