#!/bin/bash

# Author: Tristan Riehs

emacsclient -e "(kill-emacs)"
emacs --daemon
