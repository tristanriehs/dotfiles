-- IMPORTS

import XMonad
import XMonad.Config.Azerty
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Hooks.ManageDocks
import Data.Monoid
import System.Exit
import XMonad.Util.EZConfig
import XMonad.Util.Ungrab
import XMonad.Layout.Gaps
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import qualified XMonad.StackSet as W
import qualified Data.Map as M

-- VARIABLES

myBorderWidth = 2
myTerminal = "alacritty"
myModMask = mod4Mask -- mod key = super key (or windows key)
myNormalBorderColor = "#dddddd"
myFocusedBorderColor = "#4682b4"

-- STARTUP

myStartupHook = do
    spawnOnce "xsetroot -cursor_name left_ptr"
    spawnOnce "nitrogen --restore"
    spawnOnce "picom"
    spawnOnce "killall xmobar"
    spawnOnce "~/.dotfiles/.config/polybar/launch.sh"
    spawnOnce "emacs --daemon"

-- KEYBINDINGS

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [-- launch a terminal
      ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- launch gmrun
    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")

    -- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

-- LAYOUTS

myLayout = (spacingWithEdge 6 $ avoidStruts(tiled)) ||| noBorders Full ||| noBorders tiled
  where
    -- default tiling algorithm partitions the screen into two panes
    tiled   = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

-- MAIN

defaults = def
  { terminal = myTerminal,
    borderWidth = myBorderWidth,
    modMask = myModMask,
    normalBorderColor = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,

    keys = \c -> azertyKeys c <+> keys def c,
    layoutHook = myLayout,
    startupHook = myStartupHook
  }
  `additionalKeysP`
    [
    -- Applications

    -- launch Rofi drun
    ("M-r r", spawn "rofi -show run")

    -- launch Rofi power menu
    , ("M-r p", spawn "rofi -show power-menu -modi power-menu:~/.dotfiles/.config/rofi/rofi-power-menu")

    -- launch Emacs
    , ("M-a e", spawn "emacsclient -c -a 'emacs'")

    -- launch Firefox
    , ("M-a f", spawn "firefox")

    -- launch Discord
    , ("M-a d", spawn "discord")

    -- launch Steam
    , ("M-a s", spawn "steam")

    -- launch GIMP
    , ("M-a g", spawn "gimp")

    -- launch Spotify
    , ("M-a p", spawn "spotify")

    -- launch Inkscape
    , ("M-a i", spawn "inkscape")

    -- launch Telegram
    , ("M-a t", spawn "telegram-desktop")

    -- launch Htop
    , ("M-a h", spawn "alacritty -e htop")

    -- launch Alsamixer
    , ("M-a a", spawn "alacritty -e alsamixer")

    -- launch Cava
    , ("M-a c", spawn "alacritty -e cava")

    -- Sound

    -- Volume down
    , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")

    -- Volume up
    , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")

    -- Mute
    , ("<XF86AudioMute>", spawn "amixer set Master toggle")

    -- Power

    -- Shut Down
    , ("M-p s d", spawn "shutdown now")

    -- Reboot
    , ("M-p r b", spawn "reboot")
    ]

main = do
  xmproc <- spawnPipe "xmobar /home/tristan/.config/xmobar/xmobarrc"
  xmonad $ docks $ defaults
