if status is-interactive
    # aliases
    abbr -a c "clear"

    abbr -a ll "ls -lA"

    abbr -a p "sudo pacman -S"

    abbr -a gs "git status"
    abbr -a ga "git add"
    abbr -a gc "git commit -m"
    abbr -a gpll "git pull"
    abbr -a gpsh "git push"
    abbr -a gl "git log"

    # key bindings
    bind \ch 'prevd >/dev/null; commandline -f repaint'
    bind \cl 'nextd >/dev/null; commandline -f repaint'

    # valgrind
    set -x DEBUGINFOD_URLS "https://debuginfod.archlinux.org"

    pokemon-colorscripts -r
end
