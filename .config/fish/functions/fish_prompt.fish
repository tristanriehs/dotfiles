function fish_prompt
    echo -n (pwd)

    set_color blue
    echo -n " "
    set_color normal
end
