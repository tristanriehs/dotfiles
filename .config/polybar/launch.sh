# kill polybar instances
killall -q polybar

# launch polybar
polybar bar1 2>&1 | tee -a /tmp/polybar.log & disown
