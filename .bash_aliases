# misc

alias c='clear'
alias sdn='shutdown now'

# pacman

alias p='sudo pacman --color=auto -S'

# ls

alias l='ls --color=auto -Algh --group-directories-first'

# grep

alias grep='grep --color=auto'

# git

alias gs='git status'
alias ga='git add'
alias gpsh='git push'
alias gf='git fetch'
alias gc='git commit -m'
alias gpll='git pull'
alias gd='git diff'
alias gr='git rm'
alias gl='git log'
alias gll='git log --graph --branches'

# gcc

alias gcc='gcc -Werror=int-conversion -Werror=return-local-addr'

# valgrind

alias valgrind='valgrind --malloc-fill=0xAB --free-fill=0xCD'

# Emacs

alias vim='emacs -Q -nw'
