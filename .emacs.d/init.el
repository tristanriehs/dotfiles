(setq server-client-instructions nil)

(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(save-place-mode 1)

(add-hook 'before-save-hook 'whitespace-cleanup)

(electric-pair-mode 1)
(show-paren-mode 1)

(dolist (mode '(prog-mode-hook
                latex-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(column-number-mode 1)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-allow-recipe-inheritance t)

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-x K") 'kill-current-buffer)

(defun tr/project-find (file)
  "Find FILE in current project.

It is created if it does not exist."
  (find-file (concat (project-root (project-current)) file)))

(defun project-gitignore ()
  "Open current project's .gitignore file."
  (interactive)
  (tr/project-find ".gitignore"))

(global-set-key (kbd "C-c n i") 'project-gitignore)

(defun project-makefile ()
  "Open current project's Makefile."
  (interactive)
  (tr/project-find "Makefile"))

(global-set-key (kbd "C-c n m") 'project-makefile)

(defun project-license ()
  "Open current project's license."
  (interactive)
  (tr/project-find "LICENSE"))

(global-set-key (kbd "C-c n l") 'project-license)

(defun project-clear-buffers ()
  "Kill all current project buffers and start dired."
  (interactive)
  (let ((root (project-root (project-current))))
    (project-kill-buffers t) ;; no prompt
    (dired root)))

(global-set-key (kbd "C-x p K") 'project-clear-buffers)

(use-package undo-tree
  :config
  (global-undo-tree-mode))

(setq evil-want-keybinding nil
      evil-want-integration t)

(use-package evil
  :init
  (setq evil-undo-system 'undo-tree)
  (setq evil-move-beyond-eol t)
  :config
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal)

  (evil-set-initial-state 'git-commit-mode 'insert)
  (evil-set-initial-state 'magit-mode 'normal)

  (add-hook 'after-save-hook 'evil-force-normal-state)
  (evil-mode 1))

(use-package evil-collection
  :config
  (evil-collection-init))

(use-package hydra
  :config
  (defhydra hydra-tile (evil-normal-state-map "C-s-w")
    "tile windows"
    ("l" evil-window-right "right")
    ("k" evil-window-up "up")
    ("j" evil-window-down "down")
    ("h" evil-window-left "left")
    ("+" evil-window-increase-height "increase height")
    ("-" evil-window-decrease-height "decrease height")
    (">" evil-window-increase-width "decrease width")
    ("<" evil-window-decrease-width "decrease width")
    ("v" evil-window-vsplit "vertical split")
    ("s" evil-window-split "horizontal split")
    ("c" evil-quit "close")
    ("o" delete-other-windows "delete other windows")
    ("f" counsel-find-file "find file")
    ("b" counsel-switch-buffer "switch buffer")))

(require 'info)
(evil-set-initial-state 'Info-mode 'motion)

(evil-define-key 'motion Info-mode-map (kbd "n") 'Info-next)
(evil-define-key 'motion Info-mode-map (kbd "<return>") 'Info-follow-nearest-node)
(evil-define-key 'motion Info-mode-map (kbd "C-j") 'evil-scroll-line-down)
(evil-define-key 'motion Info-mode-map (kbd "C-k") 'evil-scroll-line-up)
(evil-define-key 'motion Info-mode-map (kbd "<space>") 'beacon-blink)

(add-hook 'Info-mode-hook 'efs/visual-fill)

(require 'autoinsert)
(auto-insert-mode t)

(define-skeleton org-skeleton
  "Org-mode file skeleton."
  nil
  "#+title: "
  (read-string "Document title : ") \n \n
  "#+author: Tristan Riehs\n")

(add-to-list 'auto-insert-alist '((org-mode . "Org file header") . org-skeleton))

;; (define-skeleton readme-md-skeleton
;;   "Skeleton for README.md files."
;;   "Title: "
;;   str \n
;;   "===" \n _
;;   (while t
;;     (let ((section (read-string "Section: ")))
;;       (if (length= section 0)
;;           quit
;;         (newline 3)
;;         "# " section))))

;; (add-to-list 'auto-insert-alist '(("README\\.md" . "Markdown READMEs") . readme-md-skeleton))

(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/backups")))

(defun tr/restart-emacs ()
  "Restart Emacs."
  (shell-command "~/.dotfiles/scripts/restart-emacs.sh"))

(global-set-key (kbd "C-c t r") 'tr/restart-emacs)

(use-package which-key
  :init
  (setq which-key-idle-delay 0.8)
  :config
  (which-key-mode))

(use-package hl-todo
  :config
  (setq hl-todo-keyword-faces '(("TODO" . "#EFF1F5")))
  (global-hl-todo-mode))

(defun tr/ht-todo-setup ()
  "Set up `hl-todo'."
  (set-face-attribute 'hl-todo nil
                      :background (catppuccin-get-color 'peach)
                      :foreground (catppuccin-get-color 'base)
                      :weight 'ultra-bold))

(use-package rainbow-mode)

(defun tr/run-and-switch-buffer (command)
  "Run COMMAND and `other-window'."
  (call-interactively command)
  (call-interactively 'other-window))

(defun tr/describe-function ()
  "Run `describe-function' and `other-window'."
  (interactive)
  (tr/run-and-switch-buffer 'describe-function))

(global-set-key (kbd "C-h f") 'tr/describe-function)

(defun tr/describe-variable ()
  "Run `describe-variable' and `other-window'."
  (interactive)
  (tr/run-and-switch-buffer 'describe-variable))

(global-set-key (kbd "C-h v") 'tr/describe-variable)

(defun tr/describe-key ()
  "Run `describe-key' and `other-window'."
  (interactive)
  (tr/run-and-switch-buffer 'describe-key))

(global-set-key (kbd "C-h k") 'tr/describe-key)

(defun tr/describe-mode ()
  "Run `describe-mode' and `other-window'."
  (interactive)
  (tr/run-and-switch-buffer 'describe-mode))

(global-set-key (kbd "C-h m") 'tr/describe-mode)

(defun tr/describe-package ()
  "Run `describe-package' and `other-window'."
  (interactive)
  (tr/run-and-switch-buffer 'describe-package))

(global-set-key (kbd "C-h P") 'tr/describe-package)

(defun tr/set-faces ()
  (set-face-attribute 'default nil
                      :font "Mononoki"
                      :height 128)
  (set-face-attribute 'fixed-pitch nil
                      :font "Mononoki"
                      :height 128)
  (set-face-attribute 'variable-pitch nil
                      :font "Ubuntu"
                      :height 128)
  (tr/ht-todo-setup))

(use-package catppuccin-theme
  :straight '(catppuccin-theme :type git
                               :host github
                               :repo "catppuccin/emacs"
                               :local-repo "catppuccin")
  :init
  (setq catppuccin-flavour 'latte))

(defun tr/set-theme ()
  (setq hour-at-start (decoded-time-hour (decode-time)))
  (if (and (<= 7 hour-at-start)
           (>= 21 hour-at-start))
      (setq catppuccin-flavor 'latte)
    (setq catppuccin-flavor 'frappe)))

(defun tr/emacs-client-frame-setup ()
  (add-hook 'after-make-frame-functions
            (lambda (frame)
              (with-selected-frame frame
                (tr/set-theme)
                (catppuccin-reload)
                (tr/set-faces)
                (setq doom-modeline-icon t)
                ;; ulgy but didn't find how to fix it another way yet
                (evil-set-initial-state 'git-commit-mode 'insert)))))

(defun tr/emacs-frame-setup ()
  (tr/set-theme)
  (catppuccin-reload)
  (tr/set-faces))

(use-package dashboard
  :init
  (setq dashboard-items '((recents  . 5)
                          (projects . 5))
        dashboard-startup-banner "~/.dotfiles/images/zizou.jpg"
        dashboard-projects-backend 'project-el
        dashboard-display-icons-p t
        dashboard-icon-type 'all-the-icons
        dashboard-set-heading-icons t
        dashboard-set-file-icons t)
  :config
  (dashboard-setup-startup-hook)
  (evil-define-key 'normal dashboard-mode-map (kbd "r") 'dashboard-jump-to-recents)
  (evil-define-key 'normal dashboard-mode-map (kbd "p") 'dashboard-jump-to-projects)
  (evil-define-key 'normal dashboard-mode-map (kbd "<return>") 'dashboard-return)
  (evil-define-key 'normal dashboard-mode-map (kbd "l") 'dashboard-return)
  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))))

(use-package all-the-icons)

(use-package dired
  :straight nil
  :init
  (setq dired-listing-switches "-Agho --group-directories-first"
        dired-kill-when-opening-new-dired-buffer t
        dired-omit-mode t)
  :config
  (add-hook 'dired-mode-hook 'revert-buffer))

(use-package all-the-icons-dired
  :config
  (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

(use-package dired-single
  :config
  (evil-define-key '(normal visual) dired-mode-map (kbd "l") 'dired-single-buffer)
  (evil-define-key '(normal visual) dired-mode-map (kbd "h") 'dired-single-up-directory))

(use-package dired-open
  :init
  (setq dired-open-extensions '(("png" . "gimp")
                                ("jpg" . "gimp")
                                ("svg" . "inkscape")
                                ("pdf" . "zathura"))))

(defun dired-ftouch (files)
  "Call ftouch with FILES.

Be careful, ftouch truncates existing files."
  (interactive "sFile name(s): ")
  (shell-command (concat "ftouch " files))
  (revert-buffer))

(evil-define-key '(normal motion) dired-mode-map (kbd "t") 'dired-ftouch)

(use-package doom-modeline
  :init
  (setq doom-modeline-buffer-encoding nil)
  :config
  (doom-modeline-mode 1))

(use-package rainbow-delimiters
  :config
  (add-hook 'find-file-hook 'rainbow-delimiters-mode))

(use-package beacon
  :init
  (setq beacon-dont-blink-major-modes '(t magit-status-mode vterm-mode))
  :config
  (beacon-mode 1)
  (evil-global-set-key 'normal (kbd "SPC") 'beacon-blink))

(setq tab-always-indent 'complete)
(global-set-key (kbd "M-<tab>") 'completion-at-point)
(setq dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'"))

(use-package vertico
  :init
  (setq vertico-count 6
        vertico-cylce t)
  :config
  (define-key vertico-map (kbd "C-j") 'vertico-next)
  (define-key vertico-map (kbd "C-k") 'vertico-previous)

  (savehist-mode)
  (vertico-mode))

(setq read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t
      completion-ignore-case t)

(use-package orderless
  :init
  (setq completion-styles '(orderless basic)
        completion-category-overrides '((file (styles basic partial-completion)))))

(use-package corfu
  :init
  (setq corfu-cycle t)
  :config
  (define-key corfu-map (kbd "C-j") 'corfu-next)
  (define-key corfu-map (kbd "C-k") 'corfu-previous)
  (global-corfu-mode))

(defun load-corfu-extension (extension)
  "Load the corfu extension EXTENSION."
  (load-file (concat straight-base-dir
                     "straight/"
                     straight-build-dir
                     "/corfu/extensions/corfu-"
                     extension
                     ".elc")))

(load-corfu-extension "popupinfo")
(corfu-popupinfo-mode 1)

(use-package yasnippet
  :config
  (add-to-list 'yas-snippet-dirs "~/.dotfiles/.emacs.d/custom-snippets")
  (yas-reload-all)
  (yas-global-mode 1))

;; TODO: passer à full snippets custom
(use-package yasnippet-snippets
  :config
  (yas-reload-all))

(use-package marginalia
  :config
  (marginalia-mode))

(use-package consult
  :config
  (recentf-mode)
  (global-set-key (kbd "C-s") 'consult-line)
  (global-set-key (kbd "C-x b") 'consult-buffer)
  (global-set-key (kbd "C-c b") 'consult-bookmark)
  (global-set-key (kbd "C-x p b") 'consult-project-buffer)
  (global-set-key (kbd "C-c f") 'consult-flymake))

(use-package org
  :init
  (setq org-edit-src-content-indentation 0
        org-src-tab-acts-natively t
        org-src-preserve-indentation t
        org-ellipsis " ▾"
        org-hide-emphasis-markers t)
  :config
  (set-face-attribute 'org-ellipsis nil :underline nil)

  (add-hook 'org-mode-hook 'efs/org-mode-setup)
  (add-hook 'org-mode-hook 'efs/org-font-setup)

  (evil-define-key '(normal insert visual) org-mode-map (kbd "M-j") 'org-metadown)
  (evil-define-key '(normal insert visual) org-mode-map (kbd "M-k") 'org-metaup)
  (evil-define-key 'normal org-mode-map (kbd "<tab>") 'org-cycle)

  (setq org-agenda-files '("~/projets/orgfiles/"))

  (setq org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)"))))

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch)

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.5)
                  (org-level-2 . 1.3)
                  (org-level-3 . 1.0)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
    (set-face-attribute (car face) nil :font "Ubuntu" :weight 'regular :height (cdr face)))
  (org-shifttab)

  (set-face-attribute 'org-block nil :background (catppuccin-get-color 'mantle))
  (set-face-attribute 'org-block-begin-line nil :background (catppuccin-get-color 'mantle))
  (set-face-attribute 'org-block-end-line nil :background (catppuccin-get-color 'mantle)))

(defun efs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1)
                                                          (match-end 1) "•")))))))

(use-package org-superstar
  :init
  (setq org-superstar-remove-leading-stars t
        org-superstar-headline-bullets-list '("α" "β" "γ" "δ" "ε" "ζ" "η"))
  :config
  (add-hook 'org-mode-hook 'org-superstar-mode))

(use-package visual-fill-column)
(defun efs/visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(add-hook 'org-mode-hook 'efs/visual-fill)

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (C . t)
     (shell . t)
     (haskell . t)))

  (push '("conf-unix" . conf-unix) org-src-lang-modes))

(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("cc" . "src C"))
  (add-to-list 'org-structure-template-alist '("hs" . "src haskell")))

;; Automatically tangle config files
(defun efs/org-babel-tangle-config ()
  (when (or (string-equal buffer-file-name "/home/tristan/.dotfiles/emacs.org")
            (string-equal buffer-file-name "/home/tristan/.dotfiles/xmonad.org"))
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(use-package org-make-toc
  :config
  (add-hook 'org-mode-hook 'org-make-toc-mode))

(use-package markdown-mode)
(add-hook 'markdown-mode-hook 'efs/visual-fill)

(use-package eglot
  :config
  (define-key eglot-mode-map (kbd "C-c r") 'eglot-rename))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '(latex-mode . ("texlab")))
  (add-hook 'eglot--managed-mode-hook #'eldoc-box-hover-mode t))

(use-package eldoc)

(use-package eldoc-box)

(use-package tree-sitter)
(use-package tree-sitter-langs)

(defun tr/latex-italicize-regexp ()
  "Call `query-replace-regexp' to wrap regexp with `\textit'."
  (interactive))

(add-hook 'latex-mode-hook 'tr/prog-mode-setup)
(add-hook 'latex-mode-hook 'flyspell-prog-mode)

(setq-default c-default-style "linux"
              c-indent-level 8
              c-indent-tabs-mode t
              tab-width 8)
(modify-syntax-entry ?_ "w" c-mode-syntax-table)
(require 'tree-sitter)
(set-face-attribute 'tree-sitter-hl-face:constant nil
  :slant 'italic)
(add-hook 'c-mode-hook 'tree-sitter-hl-mode)
(define-key c-mode-map (kbd "M-§") 'expand-abbrev)

(add-to-list 'load-path "/home/tristan/.emacs.d/straight/repos/emacs-dashboard")
(modify-syntax-entry ?- "w" emacs-lisp-mode-syntax-table)

(use-package fish-mode)

(add-hook 'sh-mode-hook 'tree-sitter-hl-mode)

(setq python-shell-interpreter "python3")

(defun tr/prog-mode-setup ()
  "Custom setup for programming."
  (interactive)
  (eglot-ensure)
  (flymake-mode)
  (display-line-numbers-mode)
  (message "prog-mode has been set up."))

(add-hook 'prog-mode-hook 'tr/prog-mode-setup)

(require 'gdb-mi)
(gdb-many-windows 1)

(defun project-gdb ()
  "Run `gdb' in the current project."
  (interactive)
  (let (executable (read-string "Executable : "))
    (gdb (expand-file-name (project-root (project-current)) executable))))

(require 'man)

(setq Man-notify-method 'aggressive)

(defun tr/man ()
  "Invoke `man' and switch buffer."
  (interactive)
  (call-interactively 'man)
  (other-window))

(global-set-key (kbd "C-c m") 'tr/man)

(use-package magit
  :config
  (define-key magit-status-mode-map (kbd "j") 'magit-next-line)
  (define-key magit-status-mode-map (kbd "k") 'magit-previous-line)
  (define-key magit-log-mode-map (kbd "j") 'magit-next-line)
  (define-key magit-log-mode-map (kbd "k") 'magit-previous-line))

(use-package magit-todos
  :config
  (add-hook 'magit-status-mode-hook 'magit-todos-mode))

(defun tr/emacs-client-setup ()
  (tr/emacs-client-frame-setup))

(defun tr/emacs-setup ()
  (tr/emacs-frame-setup))

(if (daemonp)
    (tr/emacs-client-setup)
  (tr/emacs-setup))
